
#include <pluginlib/class_list_macros.h>
#include <nodelet/nodelet.h>

#include <velodyne_map/heightmap.h>

namespace velodyne_map {

  class HeightMapNodelet: public nodelet::Nodelet
  {
  public:

    HeightMapNodelet() {}
    ~HeightMapNodelet() {}

    void onInit(void)
    {
      heightmap_.reset(new HeightMap(getNodeHandle(), getPrivateNodeHandle()));
    }

  private:

    boost::shared_ptr<HeightMap> heightmap_;
  };

}; // namespace velodyne_height_map

// Register this plugin with pluginlib.  Names must match nodelets.xml.
//
// parameters: class type, base class type
PLUGINLIB_EXPORT_CLASS(velodyne_map::HeightMapNodelet, nodelet::Nodelet)
